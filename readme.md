# lifestream

an MTG life-tracker desktop app which writes to a text file, for plumbing into a streaming setup

## usage

run the program and use the GUI to change your life total. it writes to a file called `life.txt`. you can use this for a text source in OBS or whatever streaming software you like.

## notes

this is really barebones but it does work, which is cool. in the future i plan to eventually support multiple life totals, custom labels (for commander damage etc), configuring output path, etc.

