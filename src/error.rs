use std::io;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("fatal error")]
    Fatal(#[from] Fatal),

    #[error("recoverable error")]
    Recoverable(#[from] Recoverable),

    #[error("im so fucking tired")]
    Other(String),
}

pub fn to_file_open(e: io::Error) -> Error {
    Error::Fatal(Fatal::FileOpen(e))
}

#[derive(Debug, Error)]
pub enum Fatal {
    #[error("error writing the file")]
    FileWrite(io::Error),
    #[error("error opening the file")]
    FileOpen(io::Error),
    #[error("the application failed to launch")]
    Launch(String),
}

#[derive(Debug, Error)]
pub enum Recoverable {
    #[error("couldn't open the icon file. it might be missing")]
    ReadIcon(#[from] lodepng::Error),
    #[error("the icon file couldn't be interpreted. it might be corrupt")]
    DecodeIcon(#[from] iced::window::icon::Error),
}
