#![windows_subsystem = "windows"]
#![feature(try_blocks)]
#![deny(missing_debug_implementations, clippy::pedantic)]
#![warn(unsafe_code)]
#![allow(clippy::cast_possible_truncation)]
use crate::error::{Fatal, Recoverable};
use iced::{
    button, executor, window::Icon, Align, Application, Button, Column, Command, Container,
    Element, Length, Settings, Text,
};
use std::{
    fs::{File, OpenOptions},
    io::{self, Read, Seek, SeekFrom, Write},
    path::Path,
};

mod error;

/// the lifestream application state.
#[derive(Debug)]
enum Lifestream {
    Loading,
    ReadError(Box<dyn std::error::Error>),
    Running(MainState),
}
impl Application for Lifestream {
    type Message = Message;
    type Flags = ();
    type Executor = executor::Default;

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        let command = Command::from(open_file("life.txt"));
        (Self::Loading, command)
    }

    fn title(&self) -> String {
        String::from("lifestream")
    }

    fn view(&mut self) -> Element<Self::Message> {
        let content = match self {
            Self::Loading => Column::new().push(Text::new("Loading").size(40)),
            Self::ReadError(e) => Column::new().push(Text::new(format!("Error :( {}", e)).size(40)),
            Self::Running(state) => {
                let minus = make_button(&mut state.minus_btn, "-", Message::Dec);
                let plus = make_button(&mut state.plus_btn, "+", Message::Inc);
                let clear = make_button(&mut state.clear_btn, "C", Message::Set(40));
                let text = Text::new(state.life.to_string()).size(54);
                Column::new()
                    .align_items(Align::Center)
                    .width(Length::Units(64))
                    .push(minus)
                    .push(text)
                    .push(plus)
                    .push(clear)
            }
        };
        Container::new(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_x()
            .center_y()
            .into()
    }

    fn update(&mut self, msg: Self::Message) -> Command<Self::Message> {
        match msg {
            Message::FileOpened(open_result) => {
                let parse_result = try {
                    let mut file = open_result?;
                    let mut txt = String::new();
                    file.read_to_string(&mut txt)?;
                    let life = if txt.is_empty() { 40 } else { txt.parse()? };
                    MainState {
                        file,
                        life,
                        plus_btn: button::State::new(),
                        minus_btn: button::State::new(),
                        clear_btn: button::State::new(),
                    }
                };
                *self = match parse_result {
                    Ok(state) => Lifestream::Running(state),
                    Err(e) => Lifestream::ReadError(e),
                };
            }
            Message::Inc => {
                self.unwrap_state().mod_life(1);
            }
            Message::Dec => {
                self.unwrap_state().mod_life(-1);
            }
            Message::Set(n) => {
                self.unwrap_state().set_life(n);
            }
        }
        Command::none()
    }
}

impl Lifestream {
    fn unwrap_state(&mut self) -> &mut MainState {
        if let Self::Running(state) = self {
            state
        } else {
            panic!("unwrap_state called on {:?} variant of Lifestream", self);
        }
    }
}

#[derive(Debug)]
struct MainState {
    life: i32,
    plus_btn: button::State,
    minus_btn: button::State,
    clear_btn: button::State,
    file: File,
}
impl MainState {
    fn write_file(&mut self) -> Result<(), crate::error::Error> {
        let res: Result<(), io::Error> = try {
            let MainState { life, file, .. } = self;
            file.set_len(0)?;
            file.seek(SeekFrom::Start(0))?;
            write!(file, "{}", life)?;
            file.sync_all()?;
        };
        Ok(res.map_err(Fatal::FileWrite)?)
    }

    fn mod_life(&mut self, amount: i32) {
        self.set_life(self.life + amount);
    }

    fn set_life(&mut self, amount: i32) {
        self.life = amount;
        if let Err(e) = self.write_file() {
            // honestly i have no idea what i was gonna do here
            // but write_file returns "Fatal" errors
            // so i guess that means we just have to crash
            panic!("{}", e);
        }
    }
}

#[derive(Debug)]
enum Message {
    Inc,
    Dec,
    Set(i32),
    FileOpened(Result<File, crate::error::Error>),
}

// iced components require messages to be Clone,
// and it can't be derived because Result
impl Clone for Message {
    fn clone(&self) -> Self {
        match self {
            Message::FileOpened(Ok(file)) => {
                Message::FileOpened(file.try_clone().map_err(crate::error::to_file_open))
            }
            Message::FileOpened(Err(err)) => {
                Message::FileOpened(Err(crate::error::Error::Other(format!("{}", err))))
            }
            Message::Set(i) => Message::Set(*i),
            Message::Inc => Message::Inc,
            Message::Dec => Message::Dec,
        }
    }
}

async fn open_file<P: AsRef<Path>>(filename: P) -> Message {
    Message::FileOpened(
        OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(filename)
            .map_err(crate::error::to_file_open),
    )
}

fn make_button<S: Into<String>>(
    state: &mut button::State,
    label: S,
    on_press: Message,
) -> Button<Message> {
    Button::new(state, Text::new(label).size(48))
        .on_press(on_press)
        .width(Length::Fill)
}

fn main() -> Result<(), crate::error::Error> {
    use rgb::ComponentBytes;

    const SIZE: (u32, u32) = (10, 256);
    let mut settings = Settings::default();

    settings.window.min_size = Some(SIZE);
    settings.window.size = SIZE;
    let attempt: Result<Icon, Recoverable> = try {
        let bitmap = lodepng::decode32_file("icon.png").map_err(Recoverable::ReadIcon)?;
        let bytes = bitmap.buffer.as_bytes().to_vec();
        let icon = Icon::from_rgba(bytes, bitmap.width as u32, bitmap.height as u32)
            .map_err(Recoverable::DecodeIcon);
        icon?
    };
    if let Ok(icon) = attempt {
        settings.window.icon = Some(icon);
    }

    Lifestream::run(settings).map_err(|iced_error| Fatal::Launch(format!("{}", iced_error)))?;

    Ok(())
}
